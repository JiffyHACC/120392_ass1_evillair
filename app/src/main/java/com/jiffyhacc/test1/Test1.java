package com.jiffyhacc.test1;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Test1 extends Activity {
    private  MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_1);
       mediaPlayer = MediaPlayer.create(this, R.raw.malak);
        mediaPlayer.start(); // no need to call prepare(); create() does that for you
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.test1, menu);
        return true;
    }

    //Button to load the camera activity
    public void CameraButton(View v){
        Button buttton = (Button) v;
        startActivity(new Intent(getApplicationContext(), Test2.class));
        mediaPlayer.stop();
    }

    //Button to load the "get quote" activity
    public void QuoteButton(View v){
        Button buttton = (Button) v;
        startActivity(new Intent(getApplicationContext(), Test3.class));
        mediaPlayer.stop();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
